# Vue Practice

Practice list goes from newest to oldest.

- [Quiz](https://sportradar.harka.com/vue-practice/203-quiz/dist/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/203-quiz/))
- [Notes](https://sportradar.harka.com/vue-practice/202-notes/dist/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/202-notes/))
- [Counter](https://sportradar.harka.com/vue-practice/201-counter/dist/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/201-counter/))
- [Simple Tweets](https://sportradar.harka.com/vue-practice/101-simple-tweets/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/101-simple-tweets/))
- [Assignment Lists - External - Refactored](https://sportradar.harka.com/vue-practice/011-assignment-lists-external-refactored/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/011-assignment-lists-external-refactored/))
- [Assignment Lists - Sum - External](https://sportradar.harka.com/vue-practice/010-assignment-lists-sum-external/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/010-assignment-lists-sum-external/))
- [Assignment Lists - Add - External - Improved](https://sportradar.harka.com/vue-practice/009-assignment-lists-add-external-improved) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/009-assignment-lists-add-external-improved/))
- [Assignment Lists - Add - External](https://sportradar.harka.com/vue-practice/008-assignment-lists-add-external/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/008-assignment-lists-add-external/))
- [Assignment Lists - External](https://sportradar.harka.com/vue-practice/007-assignment-lists-external/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/007-assignment-lists-external/))
- [Button Component - External - Improved](https://sportradar.harka.com/vue-practice/006-button-component-external-improved/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/006-button-component-external-improved/))
- [Button Component - External](https://sportradar.harka.com/vue-practice/005-button-component-external/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/005-button-component-external/))
- [Button Component - Inline](https://sportradar.harka.com/vue-practice/004-button-component-inline/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/004-button-component-inline/))
- [Assignment Lists - Inline](https://sportradar.harka.com/vue-practice/003-assignment-lists-inline/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/003-assignment-lists-inline/))
- [Button Color Changer](https://sportradar.harka.com/vue-practice/002-button-color-changer/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/002-button-color-changer/))
- [Input Two Way Binding](https://sportradar.harka.com/vue-practice/001-input-two-way-binding/) ([Source code](https://gitlab.com/sportradar-projects/sportradar/-/tree/main/vue-practice/001-input-two-way-binding/))
